#!/usr/bin/env bash

if [[ $# -eq 0 || $1 -eq '-h' || $1 -lt 0 ]] ; then
    echo -e "usage: ${0} N\n\nAllocate N MB, then release it."
else 
    N=$(free -m | grep Mem: | awk '{print int($2)}')
    if [[ $N -gt $1 ]] ;then 
      N=$1
    fi

    # consume the memory
    printf "Consuming $N MB of RAM.\n"
    yes | tr \\n x | head -c $((1024*1024*$N)) | pv -n --bytes | grep n
    sleep 2
    printf "RAM freed.\n"
fi

exit 0;
