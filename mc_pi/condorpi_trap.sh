#!/bin/sh

# function to handle termination signal
term_handler() {
    printf "[condorpi_trap] WARNING: SIGTERM caught\n"
    kill "$child" 2>/dev/null
}

trap term_handler 15

# Run the python analysisi program, wait for it to finish.
python3 calcpi3_htc.py -p ${Points} -s ${Process} &
child=$!
wait "$child"

# Do something else in your script, e.g., transfer output somewhere.
printf "[condorpi_trap] Info:  Finished.\n"

exit 0
