#!/usr/bin/env python3

'''
Estimate the value of PI using the Monte Carlo method.

Returns: NA
'''

import argparse
import random
import signal
import sys

# ==============================================================================
# Main Program
# ==============================================================================

def main(*args):
    try:

        # term_handler
        def term_handler(*args):
            print("WARNING: Signal {0} called. exiting".format(args[0]))
            print("Inside_circle: {0} Total: {1} PI ~= {2:.6f} ".format(inside_circle, ii, (inside_circle / ii) * 4))
            sys.exit(1)

	# ============================================================
	# trap signal
        signal.signal(signal.SIGTERM, term_handler)

        # ============================================================
        # command line parameters
        parser = argparse.ArgumentParser(description="Estimates the value of PI.")
        parser.add_argument("-p", "--points", action="store", type=int, dest="totalpoints", default="1000", help="Total number of points used in the MC.")
        parser.add_argument("-s", "--seed", action="store", type=int, dest="inseed", default="-1", help="Seed for random number generator, should be positive integer, if negative, current system time will be used.")
        parser.add_argument("-v", "--verbose", action="store_true", dest="verbose", help="Verbose output.")

        args = parser.parse_args()
        if args.verbose:
            print("DEBUG: Command line arguments")
            print("       total points = ", args.totalpoints)
            print("       random seed  = ", args.inseed)

        # ============================================================
        # the actual algorithm

        interval = args.totalpoints/1000
        # use current system time as seed if inseed is not defined
        if (args.inseed < 0):
            random.seed()
        else:
            random.seed(args.inseed)

        in_circle = lambda x, y: x*x + y*y < 1
        inside_circle = 0
        print("Info:  Start")
        for ii in range(0,args.totalpoints):
            if (ii%interval==0) and (ii!=0):
                print("        inside_circle: {0} Total: {1} ({2:.2f}%) PI ~= {3:.6f} ".format(inside_circle, ii, ii/args.totalpoints*100, (inside_circle / ii) * 4))
            inside_circle += in_circle(random.random(), random.random())
        # for ii
        print("Info:  Done")

        pi = (inside_circle / args.totalpoints) * 4
        print("inside circle: {0} Total: {1} PI ~= {2:.6f}".format(inside_circle, args.totalpoints, pi))

    except KeyboardInterrupt as exp:
        raise exp
    except SystemExit:
        pass
    except:
        print("WARNING: unhandled exception thrown", file=sys.stderr)
        print(sys.exc_info(), file=sys.stderr)
    else:
        return 0

if __name__ == '__main__':
    sys.exit(main(*sys.argv))
