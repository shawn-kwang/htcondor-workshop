# README

This repository holds files for a HTCondor Workshop at CGCA.

# Contents

## template

template.condor is a Condor Submission script template with lots of comments.

## hello_condor

A very (very) basic condor program. Simply prints 'Hello World'.

* hello_world.sh - shell script to print 'Hello World'.
* hello.condor - very basic condor submit script.

## mc_pi

A Monte Carlo (MC) program to calculate pi. Consists of a condor submit script, a shell script called by the submit script, and a python program which performs the calculation.

* pi.condor - condor submission script.
* condorpi.sh - shell script used by condor to run the MC.
* calcpi3_htc.py - python3 program, uses MC to caluclate pi.
* condorpi_trap.sh - alternative shell script with a trap to handle signals.

# Notes

This repostiory is used in conjunction with the [following presentation](https://tinyurl.com/y8zqwjk8) for a HTCondor workshop.

# Authors

Shawn Kwang

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

